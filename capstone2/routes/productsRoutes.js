// to use express
const express = require("express");

// to access productControllers
const productsControllers = require("../controllers/productsControllers.js");

// to access auth
const auth = require("../auth.js");

// to use router
const router = express.Router();


// Rotues for non params:


// Routes for creating products
router.post("/addProduct", auth.verify, productsControllers.addProduct);

// Routes for retrieving all products
router.get("/allProducts", auth.verify, productsControllers.getAllProducts);

// Routes for retrieving all active products
router.get("/activeProducts", productsControllers.getActiveProducts);



// Routes for params:


// Router for retrieving specific product
router.get("/:productId", productsControllers.getProduct);

// Router for updating product information
router.patch("/:productId", auth.verify, productsControllers.updateProduct);

// Routes for archiving product
router.patch("/:productId/archive", auth.verify, productsControllers.archiveProduct);





module.exports = router;