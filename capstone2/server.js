const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routes
const usersRoutes = require("./routes/usersRoutes.js");
const productsRoutes = require("./routes/productsRoutes.js");
const ordersRoutes = require("./routes/ordersRoutes.js");

// Port
const port = 4000;

const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288bacton.su474nf.mongodb.net/E-CommerceAPI?retryWrites=true&w=majority", {useNewUrlParser: true});

// Check if connected to the database
const db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));
	db.once("open", () => console.log('We are now connected to the Cloud database!'));


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Add routing of the routes from the usersRoutes
app.use("/users", usersRoutes);
// Add routing of the routes from the productsRoutes
app.use("/products", productsRoutes);
// Add routing of the routes from the ordersRoutes
app.use("/orders", ordersRoutes);



// listen on port at Terminal
app.listen(port, () => console.log(`The server is running at port ${port}!`));