
import './App.css';

import{useState, useEffect} from 'react';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Products from './pages/Products.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Logout from './pages/Logout.js'
import PageNotFound from './pages/PageNotFound.js'
import ProductView from './pages/ProductView.js'
import AdminDashBoard from './pages/AdminDashBoard.js'
import CreateNewProduct from './pages/CreateNewProduct.js'
import UpdateProduct from './pages/UpdateProduct.js'
// import CheckOut from './pages/CheckOut.js'

// importing UserProvider
import {UserProvider} from './UserContext'

// importing bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {

  let userDetails = {};
  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])

  const [user, setUser] = useState(userDetails);

  const unsetUser = () => {
    localStorage.clear();
  }
  
  useEffect(()=> {
    console.log(user);
    console.log(localStorage.getItem('token'));
  }, [user])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
          <Routes>
            <Route path = '/' element = {<Home/>}/>
            <Route path = '/products' element = {<Products/>}/>
            <Route path = '/register' element = {<Register/>}/>
            <Route path = '/login' element = {<Login/>}/>
            <Route path = '/logout' element = {<Logout/>}/>
            <Route path = '/products/:productId' element = {<ProductView/>}/>
            <Route path = '*' element = {<PageNotFound/>}/>
            <Route path = '/allProducts' element = {<AdminDashBoard/>}/>
            <Route path = '/createProduct' element = {<CreateNewProduct/>}/>
            <Route path = '/updateProduct/:productId' element = {<UpdateProduct/>}/>
            {/*<Route path = '/orders/checkout' element = {<CheckOut/>}/>*/}
          </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
