import {Container, Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
	
	return(
		<Container className = 'mt-5'>
			<Row className="row-eq-height">
			  <Col className="col-12 col-md-4 mt-3">
			    <Card className="cardHighlight h-100">
			      <Card.Img
			        variant="top"
			        src="https://www.homebuilderdigest.com/wp-content/uploads/2021/10/2753-Williams-_-Dean-Associated-Architects-Inc..png"
			        className="h-75"
			      />
			      <Card.Body>
			        <Card.Title>Renovation</Card.Title>
			        <Card.Text>
			          We offer Renovation Service if you want to renovate your existing residential house.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			  </Col>

			  <Col className="col-12 col-md-4 mt-3">
			    <Card className="cardHighlight h-100">
			      <Card.Img
			        variant="top"
			        src="https://www.seattlearchitects.org/wp-content/uploads/2022/09/06-CTA-Design-Builders-Inc.jpg"
			        className="h-75"
			      />
			      <Card.Body>
			        <Card.Title>Design and Build</Card.Title>
			        <Card.Text>
			          We offer a design and build service with our skilled architects to help you design your buildings according to your preferences.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			  </Col>

			  <Col className="col-12 col-md-4 mt-3">
			    <Card className="cardHighlight h-100">
			      <Card.Img
			        variant="top"
			        src="https://www.homebuilderdigest.com/wp-content/uploads/2021/04/Treeline-Crest-Residence-9-2018-0010.jpg"
			        className="h-75"
			      />
			      <Card.Body>
			        <Card.Title>Architecture Design</Card.Title>
			        <Card.Text>
			          If you only want the Architecture design, we also offer a dedicated package for it.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			  </Col>
			</Row>
		</Container>
		)
}