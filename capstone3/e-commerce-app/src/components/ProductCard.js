import {Container, Row, Col, Card, Button} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link, useParams} from 'react-router-dom';


export default function ProductCard(props) {

	const {user} = useContext(UserContext);

	const{_id, name, description, price, isActive} = props.productProp;

	const { archiveProduct, unarchiveProduct } = props;

	const [isDisabled, setIsDisabled] = useState(false);

	const { productId } = useParams()

	

	  return (
	    <Container>
	      <Row>
	        <Col className="col-12 mt-3">
	          <Card>
	            <Card.Body>
	              <Card.Title className="mb-3">{name}</Card.Title>
	              <Card.Subtitle>Description:</Card.Subtitle>
	              <Card.Text>{description}</Card.Text>
	              <Card.Subtitle>Price:</Card.Subtitle>
	              <Card.Text>PhP {price}</Card.Text>

	              {user === null && (
	                <Button as={Link} to="/login">
	                  Login to enroll!
	                </Button>
	              )}

	              {user && !user.isAdmin && (
	                <Button as={Link} to={`/products/${_id}`}>
	                  Details
	                </Button>
	              )}

	              {user && user.isAdmin && (
	                <>
	                  <Card.Text>Status: {isActive ? 'Active' : 'Archived'}</Card.Text>
	                  {isActive ? (
	                    <>
	                      <Button as={Link} to={`/updateProduct/${_id}`} className="me-2">
	                        Update
	                      </Button>
	                      <Button onClick={() => archiveProduct(_id)} variant="danger">
	                        Archive
	                      </Button>
	                    </>
	                  ) : (
	                    <Button onClick={() => unarchiveProduct(_id)}>Unarchive</Button>
	                  )}
	                </>
	              )}
	            </Card.Body>
	          </Card>
	        </Col>
	      </Row>
	    </Container>
	  );
}
