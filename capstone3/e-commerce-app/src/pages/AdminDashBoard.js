import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {Navigate, Link, useNavigate, useParams} from 'react-router-dom';

import {useState, useEffect, useContext} from 'react';

import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';

import ProductCard from '../components/ProductCard.js';



export default function AdminDashBoard(){
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { user, setUser } = useContext(UserContext);
  const { productId } = useParams();

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
        .then((result) => result.json())
        .then((data) => {
          setProducts(data);
          setIsLoading(false);
        })
        .catch((error) => {
          console.error(error);
          setIsLoading(false);
        });
    }, []);


  function archiveProduct(productId) {
      fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ isActive: false }),
      })
        .then((response) => {
          if (response.ok) {
            // Handle success, e.g., show a success message
            console.log('Product archived successfully');
            Swal2.fire({
              title: 'Successfully Archived!',
              icon: 'success',
              text: "You have succesfully a archive product!"
            })
          } else {
            // Handle error, e.g., show an error message
            console.error('Error archiving product');
            Swal2.fire({
                title: 'Something went wrong',
                icon: 'error',
                text: "Please try again!"
            })
          }
        })
        .catch((error) => {
          console.error('Error archiving product:', error);
        });
      const updatedProducts = products.map((product) =>
            product._id === productId ? { ...product, isActive: false } : product
          );
          setProducts(updatedProducts)  
    };

  function unarchiveProduct(productId) {
      fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
        method: 'PATCH',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ isActive: true }),
      })
        .then((response) => {
          if (response.ok) {
            // Handle success, e.g., show a success message
            console.log('Product unarchived successfully');
            Swal2.fire({
              title: 'Successfully UnArchived!',
              icon: 'success',
              text: "You have succesfully unarchived a product!"
            })
            // Refresh the products after unarchiving
            const updatedProducts = products.map((product) => 
              product._id === productId ? { ...product, isActive: true } : product
              );
            setProducts(updatedProducts);
                      
          } else {
            // Handle error, e.g., show an error message
            console.error('Error unarchiving product');
            Swal2.fire({
                title: 'Something went wrong',
                icon: 'error',
                text: "Please try again!"
            })
          }
        })
        .catch((error) => {
          console.error('Error unarchiving product:', error);
        });
        
        const updatedProducts = products.map((product) =>
              product._id === productId ? { ...product, isActive: true } : product
            );
            setProducts(updatedProducts);
    }

  return (
    user.isAdmin ? (
      <div>
        <h1 className="text-center mt-3 textClr">Admin Dashboard</h1>
        <div className="d-flex justify-content-center mt-3">
          <Button as={Link} to="/createProduct" variant="primary" className="me-2">
            Create New Product
          </Button>
          <Button variant="primary">Show All Products</Button>
        </div>
        <Container>
                  <Row>
                    {products.map((product) => (
                      <Col key={product._id} className="col-12 mt-3">
                        <ProductCard
                          productProp={product}
                          archiveProduct={archiveProduct}
                          unarchiveProduct={unarchiveProduct}
                        />
                      </Col>
                    ))}
                  </Row>
                </Container>
      </div>
    ) : (
      <Navigate to='/PageNotFound' />
    )
  );
}