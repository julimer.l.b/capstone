import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';
import ProductCard from '../components/ProductCard.js'



export default function Home(){
	
	return(
		<>
			<Banner/>
			<Highlights/>
		</>
		)
}