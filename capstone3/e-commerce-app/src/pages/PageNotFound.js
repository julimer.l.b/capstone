import React from 'react';
import {Link} from 'react-router-dom';

// import {Route, Link, Routes} from 'react-router-dom';


export default function PageNotFound(){

	return(

		<div className = "text-center pt-5">
			<h1 className="textClr">404 Page not Found!</h1>
			<p className="textClr">Go back to <Link to = "/" style = {{textDecoration: 'none'}}>homepage</Link>.</p>
		</div>

		)

}