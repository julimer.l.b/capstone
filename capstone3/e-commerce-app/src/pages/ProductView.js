import {useState, useEffect, useContext} from 'react';

import {useParams, useNavigate, Link} from 'react-router-dom';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';

import {Container, Row, Col, Button, Card} from 'react-bootstrap';

export default function ProductView(){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
  const {user} = useContext(UserContext)
  const navigate = useNavigate();
	const { productId } = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
    // .catch(error => {
    //   console.log('Error fetching product:', error)
    // })
	}, [productId])


    const checkOut = (productId) => {
      /*console.log('Form submitted');
      console.log(process.env.REACT_APP_API_URL);
      console.log(localStorage.getItem('token'));*/

      // we are going to fetch the route going to the enroll on our backend application.
      fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          product: {
            productId: productId
          }
        })
      })
        .then(response => response.json())
        .then(data => {
          console.log(data);
          if (data) {
            Swal2.fire({
              title: 'Successfully enrolled',
              icon: 'success',
              text: "You have successfully enrolled for this course!"
            });
            navigate('/products');
          } else {
            Swal2.fire({
              title: 'Something went wrong',
              icon: 'error',
              text: "Please try again!"
            });
          }
        })
        .catch(error => {
          console.log('Error during checkout:', error);
          console.log('Response object:', error.response);
          console.log('Error object:', error);
        });
    };

    return (
      <Container>
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

                <div className="d-flex justify-content-between">
                  {user ? (
                    <Button variant="primary" onClick={() => checkOut(productId)}>
                      Enroll Now!
                    </Button>
                    ) : (
                    <Button as={Link} to="/login" variant="primary">
                      Login to Enroll
                    </Button>
                    )}
                  <Button as={Link} to="/products" variant="primary" className="me-2">
                    Back
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
}